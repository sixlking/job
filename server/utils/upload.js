// 引入图片上传插件
const multer = require('multer')
const path = require('path')
const moment = require('moment')
const mkdirp = require('mkdirp')

const storage = multer.diskStorage({
    // 配置文件目录
    destination: async (req, file, cb) => {
        // 获取当前日期
        const today = moment().format('YYYYMMDD');

        // 按照日期生成图片存储目录
        const dir = path.join('static/uploads', today);

        // 创建文件目录
        await mkdirp(dir)

        // 上传之前确保dir是存在的
        cb(null, dir)
    },
    // 配置文件名
    filename: function (req, file, cb) {
        // 获取文件后缀名
        const extname = path.extname(file.originalname);

        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)

        cb(null, file.fieldname + '-' + uniqueSuffix + extname);
    }
})

const upload = multer({
    storage: storage
})

module.exports = upload;