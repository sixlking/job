// 引入expres
const express = require("express")
// 引入路由模块
const router = express.Router()
// 引入数据库
const connection = require("../db/sql.js")
// 引入时间处理插件
const moment = require("moment")

const {
    ERROR_MSG
} = require("../config/global")

// 获取公司信息卡片数据接口
router.get("/jobType", (req, res) => {
    const jobTypeSQL = "select * from d_job_type"
    connection.query(jobTypeSQL, (err, result) => {
        if (result) {
            res.reply_success(result)
        } else {
            res.reply_error(ERROR_MSG.BUSINESS_ERROR, err.message)
        }
    })
})

// 获取公司信息卡片数据接口
router.get("/education", (req, res) => {
    const educationSQL = "select * from d_education"
    connection.query(educationSQL, (err, result) => {
        if (result) {
            res.reply_success(result)
        } else {
            res.reply_error(ERROR_MSG.BUSINESS_ERROR, err.message)
        }
    })
})

// 获取公司信息卡片数据接口
router.get("/experience", (req, res) => {
    const experienceSQL = "select * from d_job_experience"
    connection.query(experienceSQL, (err, result) => {
        if (result) {
            res.reply_success(result)
        } else {
            res.reply_error(ERROR_MSG.BUSINESS_ERROR, err.message)
        }
    })
})

// 获取公司标签数据接口
router.get("/tag", (req, res) => {
    const tagSQL = "select * from d_job_tag"
    connection.query(tagSQL, (err, result) => {
        if (result) {
            res.reply_success(result)
        } else {
            res.reply_error(ERROR_MSG.BUSINESS_ERROR, err.message)
        }
    })
})

// 发布职位
router.post("/publishJob", (req, res) => {
    const job = req.body;
    const publishJobSQL = `INSERT INTO s_job (c_id,name,salary,education,experience,duty,requirements,type,city,address,tag,create_time) VALUES (${job.companyId},'${job.name}','${job.salary}',${job.education},${job.experience},'${job.duty}','${job.requirements}',${job.type},'${job.city}','${job.address}','${job.tag}','${moment().format("YYYY-MM-DD")}')`;
    connection.query(publishJobSQL, (err, result) => {
        if (err) {
            console.log(err)
        } else {
            res.reply_success()
        }
    })
})

module.exports = router;