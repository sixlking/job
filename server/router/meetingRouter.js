// 引入expres
const express = require("express");
// 引入路由模块
const router = express.Router();

// 引入全局消息
const {
    ERROR_MSG
} = require("../config/global");

const {
    getRooms
} = require('../config/socket')
let connectedUsers = []; //参会人员信息
let rooms = [] //房间信息

// 获取房间是否存在
router.post("/room-exist", (req, res) => {
    const rooms = getRooms()
    const roomId = req.body.roomId;
    // 查询房间信息是否存在
    const room = rooms.find(r => r.id === roomId)
    if (room) {
        if (connectedUsers.length > 3) {
            res.reply_success({
                roomExist: true,
                connectedFull: true
            })
        } else {
            res.reply_success({
                roomExist: true,
                connectedFull: false
            })
        }
    } else {
        res.reply_success({
            roomExist: false
        })
    }
})

module.exports = router;