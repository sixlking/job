/*
 * 文件废弃，不再使用   
 */
// 引入mongoose
const mongoose = require('mongoose');

// 定义链接地址
const mongoURI = 'mongodb://127.0.0.1:27017/job';

// 建立连接
mongoose.connect(mongoURI)

// 获取链接信息判断是否链接成功
const db = mongoose.connection;
db.on('error',console.error.bind(console, 'MongoDB connect Fail~:'))
db.once('open',() => {
    console.log("^_^MongoDB connect success!^_^");
})