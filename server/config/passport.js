/*
 * @Author: llk
 * @Date: 2022-04-03 01:17:44
 * @LastEditTime: 2022-04-03 01:55:08
 * @LastEditors: llk
 * @Description: 配置passport
 * @FilePath: \job\server\config\passport.js
 */
const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

const connection = require('../db/sql.js')
// const mongoose = require('mongoose')
// 引入用户模型
// const User = require("../model/userModel")

const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = 'secret';
module.exports = passport => {
    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {

        const findOndSQL = `SELECT * FROM s_user WHERE s_user.id = ${jwt_payload.id}`;
        connection.query(findOndSQL, (err, result) => {
            if (err) {
                console.log(err)
            }
            if (result.length > 0) {
                return done(null, user)
            }
            return done(null, false)
        })
    }));
}