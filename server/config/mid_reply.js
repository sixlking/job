/*
 * @Author: llk
 * @Date: 2022-04-02 22:54:10
 * @LastEditTime: 2022-04-03 00:21:56
 * @LastEditors: llk
 * @Description: 封装请求回复的中间件
 * @FilePath: \job\server\utils\mid_reply.js
 */
const moment = require('moment')

// 请求成功的回复
function reply_success(req, res, next) {
    res.reply_success = function (result = "OK") {
        res.send({
            "status": true,
            result,
            "timestamp": moment().utc().valueOf()
        })
    }
    next()
}

// 请求失败的回复
function reply_error(req, res, next) {
    res.reply_error = function (error, desc) {
        // 如果有描述则用使用提供的描述，否则使用默认描述
        if (desc) {
            error['error_description'] = desc
        }

        res.send({
            "status": false,
            error,
            "timestamp": moment().utc().valueOf()
        })
    }
    next()
}

module.exports = {
    reply_success,
    reply_error
}