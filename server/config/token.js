const {
    ERROR_MSG
} = require("./global");
// 引入token模块
const jwt = require('jsonwebtoken');

//用于生成和解析token
const signkey = '!web1_job_project_$^_^$';

const verifyToken = (res, token) => {
    //删除不属于token值
    const dealToken = token.substr(7);

    let result = {};

    jwt.verify(dealToken, signkey, function (err, data) {
        // 如果token失效直接返回
        if (err) {
            return res.reply_error(ERROR_MSG.SIGN_CHECK_ERROR, "TOKEN无效")
        } else {
            result = data;
        }
    })

    return result;
}

module.exports = {
    signkey,
    verifyToken
}