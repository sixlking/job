/*
 * @Author: llk
 * @Date: 2022-04-02 23:24:38
 * @LastEditTime: 2022-04-02 23:49:40
 * @LastEditors: llk
 * @Description: 
 * @FilePath: \job\server\utils\global.js
 */


const ERROR_MSG = {
    'UNKNOWN_ERROR': {
        "error_code": -1,
        "error_reason": "unknown_error",
        "error_description": "未知错误"
    },
    'SERVER_ERROR': {
        "error_code": 5000,
        "error_reason": "server_error",
        "error_description": "服务器内部异常"
    },
    "BUSINESS_ERROR": {
        "error_code": 5001,
        "error_reason": "business_error",
        "error_description": "业务错误"
    },
    "ILLEGAL_ARGUMENT_ERROR": {
        "error_code": 5002,
        "error_reason": "illegal_argument_error",
        "error_description": "参数错误"
    },
    "JSON_SERIALIZATION_ERROR": {
        "error_code": 5003,
        "error_reason": "json_serialization_error",
        "error_description": "JSON序列化失败"
    },
    "UNAUTHORIZED_ACCESS": {
        "error_code": 5004,
        "error_reason": "unauthorized_access",
        "error_description": "未经授权的访问"
    },
    "SIGN_CHECK_ERROR": {
        "error_code": 5005,
        "error_reason": "sign_check_error",
        "error_description": "签名校验失败"
    },
    "FEIGN_CALL_ERROR": {
        "error_code": 5006,
        "error_reason": "feign_call_error",
        "error_description": "远程调用失败"
    }
}


module.exports = {
    ERROR_MSG
}