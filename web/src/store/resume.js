const resume = {
  namespaced: true,
  state: {
    resumeList: {
        user: {
            name: "",
            age: "",
            phone: "",
            mail: "",
            education: "",
            professionalAbility: "",
            projectExperience: "",
            positionSalary: "",
            personalSummary: "",
          },

          carouselImg:[
            require("@/assets/imgs/resume/resume2.jpg"),
            require("@/assets/imgs/resume/resume3.jpg"),
            require("@/assets/imgs/resume/resume4.jpg"),
             require("@/assets/imgs/resume/resume5.jpg"),
          ],

          templateImg:[
            require("@/assets/imgs/resume/resume6.jpg"),
            require("@/assets/imgs/resume/resume7.jpg"),
            require("@/assets/imgs/resume/resume8.jpg"),
          ]


    },
  },

  mutations: {
    RESUMEDATA(state, val) {
      state.resumeList = val;
    },
  },
  getters: {},
  actions: {},
};

export default resume;
