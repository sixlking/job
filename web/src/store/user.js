const user = {
    namespaced: true,
    state: {
        token: sessionStorage.getItem('token'),
        info: JSON.parse(sessionStorage.getItem('info')),
        loginModal: false,
        personalModal: false
    },
    getters: {},
    mutations: {
        SET_INFO(state, val) {
            state.info = val
        },
        SET_TOKEN(state, val) {
            state.token = val
        },
        SET_LOGINMODAL(state, val) {
            state.loginModal = val
        },
        SET_PERSONALMODAL(state, val) {
            state.personalModal = val;
        }
    },
    actions: {}
}

export default user;