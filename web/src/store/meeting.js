// 会议相关vuex
const meeting = {
    namespaced: true,
    state: {
        isRoomHost: false, //是否是会议的主持人
        isOnlyAudio:false, // 是否仅开启音频
        meettingUserName:'', //参会人员名称
        roomId:'', // 会议ID
        participants:[], // 会议参与人
        streamSource:[], // 媒体流
        shareStream:null,// 分享的媒体流
    },
    getters: {

    },
    mutations: {
        SET_ISROOMHOST(state,val){
            state.isRoomHost = val
        },
        SET_ISONLYAUDIO(state,val){
            state.isOnlyAudio = val
        },
        SET_MEETING_USERNAME(state,val){
            state.meettingUserName = val
        },
        SET_ROOMID(state,val){
            state.roomId = val
        },
        SET_PARTICIPANTS(state,val){
            state.participants.push(val)
        },
        SET_STREAMSOURCE(state,val){
            state.streamSource.push(val)
        },
        REMOVE_STREAMSOURCE(state,connectUserSocketId){
            const index = state.streamSource.findIndex(stream => {
                return stream.connectUserSocketId == connectUserSocketId
            })
            if (index !== -1) {
                state.streamSource.splice(index,1)
            }
            
        },
        SET_SHARESTREAM(state,stream){
            state.shareStream = stream
        }
    },
    actions: {

    }
}

export default meeting;