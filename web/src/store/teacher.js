const teacher = {
  namespaced: true,
  state: {
    resumeId: 0,
    tableData: [
      {
        jobTitle: "产品经理",
        jobRemark: "",
        jobArea: "芜湖",
        salary: "4k - 6k",
        experience: "1",
        education: "2",
      },
      {
        jobTitle: "测试",
        jobRemark: "",
        jobArea: "芜湖",
        salary: "4k - 6k",
        experience: "1",
        education: "2",
      },
      {
        jobTitle: "web前端",
        jobRemark: "",
        jobArea: "芜湖",
        salary: "4k - 6k",
        experience: "1",
        education: "2",
      },
      {
        jobTitle: "主管",
        jobRemark: "",
        jobArea: "芜合肥",
        salary: "6k - 9k",
        experience: "1",
        education: "2",
      },
      {
        jobTitle: "销售",
        jobRemark: "",
        jobArea: "南京",
        salary: "4k - 6k",
        experience: "1",
        education: "2",
      },
      {
        jobTitle: "主管",
        jobRemark: "",
        jobArea: "芜合肥",
        salary: "6k - 9k",
        experience: "1",
        education: "2",
      },
      {
        jobTitle: "产品经理",
        jobRemark: "",
        jobArea: "芜湖",
        salary: "4k - 6k",
        experience: "1",
        education: "2",
      },
    ],
    resumeTableData: [
      {
        id: 10001,
        isCheck: "0",
        isPass: "0",
        name: "Test1",
        job: "web前端",
        sex: "1",
        age: 28,
        address: "test abc",
      },
      {
        id: 10002,
        isCheck: "1",
        isPass: "0",
        name: "Test2",
        job: "java工程师",
        sex: "0",
        age: 28,
        address: "test abc",
      },
      {
        id: 10003,
        isCheck: "1",
        isPass: "1",
        name: "Test3",
        job: "H5工程师",
        sex: "1",
        age: 28,
        address: "test abc",
      },
    ],
  },
  mutations: {
    DELIVERID(state, val) {
      state.resumeId = val;
    },
  },
  getters: {},
  actions: {},
};

export default teacher;
