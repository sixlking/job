/*
 * @Author: llk
 * @Date: 2022-04-01 01:28:09
 * @LastEditTime: 2022-05-14 22:05:49
 * @LastEditors: qianduanxuyaochendian lxy_01@outlook.com
 * @Description: Vuex主文件
 * @FilePath: \job\web\src\store\index.js
 */

import { createStore } from "vuex"
import auth from "./auth"
import user from "./user"
import meeting from "./meeting"
import company from "./company"
import job from "./job"
import position from "./position"
import resume from "./resume"
import teacher from "./teacher"
import workplaceinformation from "./workplaceinformation"
export default createStore({
  modules: {
    auth,
    user,
    meeting,
    job,
    company,
    position,
    resume,
    teacher,
    workplaceinformation,
  },
})
