/*
 * @Author: your name
 * @Date: 2022-04-16 17:55:15
 * @LastEditTime: 2022-05-16 10:20:37
 * @LastEditors: qianduanxuyaochendian lxy_01@outlook.com
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \job-master\web\src\store\job.js
 */

const job = {
  namespaced: true,
  state: {
    jobList: [
      {
        positionName: "产品助理",
        salary: "5-7k",
        workCity: "广州",
        workExperience: "一年以内",
        Academic: "大专以上",
        company_info: {
          avatarUrl: "../assets/imgs/company/job_company.png",
          name: "寅时科技",
          type: "计算机软件",
          status: "未融资",
        },
      },
      {
        positionName: "产品助理",
        salary: "5-7k",
        workCity: "广州",
        workExperience: "一年以内",
        Academic: "大专以上",
        company_info: {
          name: "寅时科技",
          type: "计算机软件",
          status: "未融资",
        },
      },
      {
        positionName: "产品助理",
        salary: "5-7k",
        workCity: "广州",
        workExperience: "一年以内",
        Academic: "大专以上",
        company_info: {
          name: "寅时科技",
          type: "计算机软件",
          status: "未融资",
        },
      },
      {
        positionName: "产品助理",
        salary: "5-7k",
        workCity: "广州",
        workExperience: "一年以内",
        Academic: "大专以上",
        company_info: {
          name: "寅时科技",
          type: "计算机软件",
          status: "未融资",
        },
      },
      {
        positionName: "产品助理",
        salary: "5-7k",
        workCity: "广州",
        workExperience: "一年以内",
        Academic: "大专以上",
        company_info: {
          name: "寅时科技",
          type: "计算机软件",
          status: "未融资",
        },
      },
      {
        positionName: "产品助理",
        salary: "5-7k",
        workCity: "广州",
        workExperience: "一年以内",
        Academic: "大专以上",
        company_info: {
          name: "寅时科技",
          type: "计算机软件",
          status: "未融资",
        },
      },
      {
        positionName: "产品助理",
        salary: "5-7k",
        workCity: "广州",
        workExperience: "一年以内",
        Academic: "大专以上",
        company_info: {
          name: "寅时科技",
          type: "计算机软件",
          status: "未融资",
        },
      },
      {
        positionName: "产品助理",
        salary: "5-7k",
        workCity: "广州",
        workExperience: "一年以内",
        Academic: "大专以上",
        company_info: {
          name: "寅时科技",
          type: "计算机软件",
          status: "未融资",
        },
      },
    ],
    jobData: JSON.parse(localStorage.getItem("jobData")) || [],
  },
  mutations: {
    ADD(state, data) {
      state.jobData.push(data)
    },
  },
  getters: {},
  actions: {},
}

export default job
