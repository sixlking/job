/*
 * @Author: qianduanxuyaochendian lxy_01@outlook.com
 * @Date: 2022-05-14 22:04:01
 * @LastEditors: qianduanxuyaochendian lxy_01@outlook.com
 * @LastEditTime: 2022-05-16 10:10:34
 * @FilePath: \job\web\src\store\workplaceinformation.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
const workplaceinformation = {
  namespaced: true,
  state: {
    infoList: [
      {
        id: 0,
        title: "领导收走你的工作，就是逼你离职！",
        content: require("@/assets/imgs/workplaceInfo/0.png"),
      },
      {
        id: 1,
        title: "什么样的公司才能留住90后？",
        content: require("@/assets/imgs/workplaceInfo/1.png"),
      },
      {
        id: 2,
        title: "5成人上下班的交通费在10元内",
        content: require("@/assets/imgs/workplaceInfo/2.png"),
      },
      {
        id: 3,
        title: "甘肃：施工人员考核合格才能上岗",
        content: require("@/assets/imgs/workplaceInfo/3.png"),
      },
      {
        id: 4,
        title: "近5成人每天通勤1—2小时，赚钱太难了！",
        content: require("@/assets/imgs/workplaceInfo/4.png"),
      },
      {
        id: 5,
        title: "遇到这些吸血型公司，一定要快跑！",
        content: require("@/assets/imgs/workplaceInfo/5.png"),
      },
      {
        id: 6,
        title: "不解决这些问题，工作越久越贬值",
        content: require("@/assets/imgs/workplaceInfo/6.png"),
      },
    ],
    info: {},
  },
  mutations: {
    GETINFO(state, id) {
      state.info = state.infoList[id]
    },
  },
  getters: {},
}
export default workplaceinformation
