import axios from "../request";

// 按需导出每个请求，也就是按需导出每个api

// 请求首页数据
export const GetHomeAPI = (params) => axios.get('/index/index', {
    params
});

// 登录接口
export const LoginAPI = (params) => axios.post('/user/login', params);


// 获取用户信息
export const UserInfoAPI = () => axios.get('/user/userInfo')