import axios from "../request"

// 请求用户权限
export const getAuthAPI = () => axios.get("/user/auth")