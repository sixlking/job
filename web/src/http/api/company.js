/*
 * @Author: qianduanxuyaochendian lxy_01@outlook.com
 * @Date: 2022-05-19 16:35:30
 * @LastEditors: qianduanxuyaochendian lxy_01@outlook.com
 * @LastEditTime: 2022-05-24 22:59:21
 * @FilePath: \job\web\src\http\api\company.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import axios from "../request"

// 请求首页数据
export const RegisterCompanyAPI = (params) =>
  axios.post("/company/register", params)

//请求公司列表数据
export const getCompanyList = () => axios.get("/company/getCompanyList")

// 请求职位列表数据
export const getJobList = () => axios.get("/company/getJobList")

// 获取分类职位列表数据
export const getCategory = (params) =>
  axios.post("/company/getCategoryList", params)

//获取logo和name的接口
export const getAvatar = (params) => axios.post("/company/obtainAvatar", params)

//实现首页首页搜索功能的接口
export const searchJob = (params) => axios.post("/company/search", params)

// 获取公司卡片简要信息接口
export const getCompanySimpleInfoAPI = () =>
  axios.get("/company/companySimpleInfo")

// 获取公司详细信息接口
export const getCompanyInfoAPI = (params) =>
  axios.get("/company/companyInfo", { params })

//获取分类列表接口
export const loadListAPI = () => axios.get("/company/loadList")

//获取工作详情的接口
export const getDetailAPI = (params) => axios.post("/company/getDetail", params)

//获取简历列表的接口
export const getResumeListAPI = (params) =>
  axios.post("/company/getResumeList", params)
//发送邮件接口
export const sendEmailAPI = (params) => axios.post("/company/sendEmail", params)

//获取学生简历信息
export const getStudentResumeAPI = (params)=>axios.post('/company/getStudentResume',params)
