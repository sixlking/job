/*
 * @Author: qianduanxuyaochendian lxy_01@outlook.com
 * @Date: 2022-05-20 13:16:34
 * @LastEditors: qianduanxuyaochendian lxy_01@outlook.com
 * @LastEditTime: 2022-05-24 22:22:46
 * @FilePath: \job\web\src\http\api\student.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

import axios from "../request"

//获取学生简历信息
export const getStudentInfo = (params) =>
  axios.post("/student/getStudentInfo", params)
// 修改学生简历信息
export const editStudentInfo = (params) =>
  axios.post("/student/editStudentInfo", params)

//获取学生用户信息(头像)
export const getStudentPicture = (params) =>
  axios.post("/student/getStudentPicture", params)

//获取面试题数据
export const getTopic = (params) => axios.post("/student/getTopic", params)

// 增加面试题
export const addTopic = (params) => axios.post("/student/addTopic", params)

// 更新面试题
export const editTopic = (params) => axios.post("/student/editTopic", params)

//删除面试题
export const deleteTopic = (params) =>
  axios.post("/student/deleteTopic", params)

//新增简历信息
export const addResumeAPI = (params) => axios.post("/student/addResume", params)

//投递简历
export const sendResumeAPI = (params) =>
  axios.post("/student/sendResume", params)

//获取当前学生用户投递的全部简历信息
export const getResumeListAPI = (params) =>
  axios.post("/student/getResumeList", params)
