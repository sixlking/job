import axios from "../request"

// 请求工作类型列表数据
export const getJobTypeListAPI = () => axios.get("/job/jobType")

// 请求学历列表数据
export const getEducationListAPI = () => axios.get("/job/education")

// 请求工作经验列表数据
export const getExperienceListAPI = () => axios.get("/job/experience")

// 请求工作标签列表数据
export const getTagListAPI = () => axios.get("/job/tag")

// 发布职位
export const publishJobAPI = (params) => axios.post("/job/publishJob",params)