/*
 * @Author: qianduanxuyaochendian lxy_01@outlook.com
 * @Date: 2022-05-14 16:18:44
 * @LastEditors: qianduanxuyaochendian lxy_01@outlook.com
 * @LastEditTime: 2022-05-14 16:20:15
 * @FilePath: \job\web\src\http\api\register.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

// 引入封装的axios
import axios from "../request"

// 注册接口
export const RegisterAPI = (params) => axios.post("/user/register", params)
