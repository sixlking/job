/*
 * @Author: llk
 * @Date: 2022-04-03 22:42:34
 * @LastEditTime: 2022-04-03 22:55:31
 * @LastEditors: llk
 * @Description: 封装axios
 * @FilePath: \job\web\src\plugins\request.js
 */
import axios from 'axios'

const instance = axios.create({
    baseURL: "http://127.0.0.1:5000",
    timeout: 5000
});

// 请求拦截器
instance.interceptors.request.use(
    config => {
        // 配置token
        if (sessionStorage.getItem('token')) {
            config.headers.Authorization = sessionStorage.getItem('token');
        }
        return config;
    },
    err => {
        return Promise.reject(err)
    }
)

// 响应拦截器
instance.interceptors.response.use(
    response => {
        // 如果后端返回数据中的status是true则直接将数据返回出去
        if (response.data.status) {
            return Promise.resolve(response.data);
        } else {
            // 如果后端返回数据中status是false,则把错误信息弹出提示，并且将数据返回到Promise的reject中
            window.$message.error(response.data.error.error_description)
            return Promise.reject(response.data)
        }
    },
    err => {
        return Promise.reject(err)
    }
)

export default instance;