import io from 'socket.io-client';
import store from '@/store'
import {
    responseRTCConnection,handleSignalingData,removePeerConnection
} from './webRTC'

// 设置服务端地址
const SERVER = 'http://127.0.0.1:5000';

// 连接到服务器
let socketObj = null;
export const connectSocketServer = () => {
    socketObj = io(SERVER);
    // socketObj.on('connection', () => {
    //     console.log('建立socket连接成功');
    //     console.log(socketObj.id);
    // });

    // 监听服务端发送的roomId这个事件，并接收参数，将传递过来的参数中的roomId保存到vuex
    socketObj.on('roomId', (data) => {
        const {
            roomId
        } = data;
        store.commit('meeting/SET_ROOMID', roomId)
    })

    socketObj.on('roomUpdate', (data) => {
        const {
            connectedUsers
        } = data;
        store.commit('meeting/SET_PARTICIPANTS', connectedUsers)
    })

    socketObj.on('rtcApply', (data) => {
        const {
            connectUserSocketId
        } = data;

        // 已在当前会议房间的用户响应新用户发起的webRTC对等连接,
        // 第二个参数false表示当前状态是发起方在等待接收方响应webRTC
        responseRTCConnection(connectUserSocketId,false)

        // 通知发起方已经准备完毕，可以进行webRTC连接
        socketObj.emit('conn-init',{connectUserSocketId})
    })

    socketObj.on('conn-signal',data => {
        handleSignalingData(data)
    })

    socketObj.on('conn-init',data => {
        // 注意这里是接收方的socketId的值
        const {connectUserSocketId}  = data;
        responseRTCConnection(connectUserSocketId,true)
    })

    socketObj.on('user-leave',data => {
        removePeerConnection(data)
    })
}

// 创建房间
export const createRoom = (meettingUserName) => {
    // 向服务器发送一个事件并传递相关参数，事件名随意
    socketObj.emit('createRoom', {
        meettingUserName
    })
}

// 加入房间的方法
export const joinRoom = (roomId, meettingUserName) => {
    socketObj.emit('joinRoom', {
        roomId,
        meettingUserName
    })
}

// 发送信令数据到服务器
export const signalPeerData = (signalData) => {
    socketObj.emit('conn-signal',signalData)
}