/*
 * @Author: qianduanxuyaochendian lxy_01@outlook.com
 * @Date: 2022-05-07 09:49:56
 * @LastEditors: qianduanxuyaochendian lxy_01@outlook.com
 * @LastEditTime: 2022-05-09 21:07:16
 * @FilePath: \job\web\src\main.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { createApp } from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import native from "@/plugins/native"
//引入echarts
import * as echarts from "echarts"
import animate from "animate.css"
// 引入封装svg组件
import SvgIcon from "./components/SvgIcon.vue"
import "./icons"
// 引入视频播放插件
import vue3videoPlay from "vue3-video-play" // 引入组件
import "vue3-video-play/dist/style.css" // 引入css
//引入表格插件
import "xe-utils"
import VXETable from "vxe-table"
import "vxe-table/lib/style.css"
// 引入动画
import "hover.css"
const app = createApp(App)
app.config.globalProperties.$echarts = echarts
app.config.globalProperties.$XModal = VXETable.modal
app
  .component("svg-icon", SvgIcon)
  .use(native)
  .use(store)
  .use(router)
  .use(animate)
  .use(VXETable)
  .use(vue3videoPlay)
  .mount("#app")
