/*
 * @Author: llk
 * @Date: 2022-04-01 01:28:27
 * @LastEditTime: 2022-04-02 22:16:16
 * @LastEditors: llk
 * @Description: 路由相关
 * @FilePath: \joint\web\src\router\index.js
 */
import { createRouter, createWebHistory } from "vue-router"
import basicRoutes from "./basicRoutes"
import store from "@/store"

const router = createRouter({
  history: createWebHistory(),
  routes: basicRoutes,
})

router.afterEach((to, from) => {
  if (
    (from.path === "/resume-guidance/resume-preview-one" ||
      from.path === "/resume-guidance/resume-preview-two" ||
      from.path === "/resume-guidance/resume-preview-three") &&
    to.path === "/resume-guidance/resume-template"
  ) {
    window.location.reload()
  }
})

router.beforeEach(async (to, from, next) => {
  // 当前有没有获取过权限，如果获取过了 就不要在获取了
  if (to.fullPath !== '/job-square' && !store.state.auth.hasAuthRoutes) {
    // 获取权限,获取权限，调用获取权限的接口
    await store.dispatch("auth/getAuthRoute")
    let r = await store.dispatch("auth/getMenuList") // 去action中获取数据
    console.log('r',r);
    r.forEach((el) => {
      router.addRoute("LAYOUT", el)
    })

    next({
      ...to,
      replace: true,
    })
  } else {
    next() // 如果已经获取了权限就可以访问页面了
  }
})

export default router
