import notify from "@/views/notify/Index.vue"

const authRoutes = [
  // 仪表盘路由
  {
    path: "dashboard",
    name: "DASHBOARD",
    component: () => import("@/views/dashboard/Index.vue"),
    meta: {
      icon: require("@/icons/svg/dashboard.svg"),
      title: "仪表盘",
    },
  },
  //在线视频模块路由
  {
    path: "/meeting",
    name: "MEETING",
    component: () => import("@/views/meeting/Index.vue"),
    redirect: "/meeting/identity-choice",
    meta: {
      icon: "",
      title: "会议",
    },
    children: [
      {
        path: "identity-choice",
        name: "IDENTITYCHOICE",
        component: () => import("@/views/meeting/IdentityChoice.vue"),
        isHidden: true,
        meta: {
          icon: "",
          title: "身份选择",
        },
      },
      {
        path: "room-info",
        name: "ROOMINFO",
        component: () => import("@/views/meeting/RoomInfo.vue"),
        isHidden: true,
        meta: {
          icon: "",
          title: "房间信息",
        },
      },
      {
        path: "room",
        name: "ROOM",
        component: () => import("@/views/meeting/Room.vue"),
        isHidden: true,
        meta: {
          icon: "",
          title: "会议室",
        },
      },
    ],
  },
  //简历模块路由开始
  {
    path: "/resume-guidance",
    name: "RESUMEGUIDANCE",
    redirect: "/resume-guidance/resume-template",
    component: () => import("@/views/resume-guidance/Index.vue"),
    meta: {
      icon: "",
      title: "简历指导",
    },
    children: [
      {
        path: "resume-template",
        name: "RESUMETEMPLATE",
        component: () => import("@/views/resume-template/Index.vue"),
        meta: {
          icon: "",
          title: "简历模板",
        },
      },
      {
        path: "resume-explain",
        name: "RESUMEEXPLAIN",
        component: () => import("@/views/resume-explain/Index.vue"),
        meta: {
          icon: "",
          title: "简历讲解",
        },
      },
      {
        path: "resume-generation",
        name: "RESUMEGENERATION",
        component: () => import("@/views/resume-generation/Index.vue"),
        meta: {
          icon: "",
          title: "在线简历生成",
        },
      },
      {
        path: "resume-preview-one",
        name: "RESUMEPREVIEWONE", //默认第一个简历模板
        isHidden:true,
        component: () =>
          import("@/views/resume-guidance/components/resume-preview-one.vue"),
        meta: {
          icon: "",
          title: "简历预览",
        },
      },
      {
        path: "resume-preview-two",
        name: "RESUMEPREVIEWTWO",
        isHidden:true,
        component: () =>
          import("@/views/resume-guidance/components/resume-preview-two.vue"),
        meta: {
          icon: "",
          title: "简历预览TWO ",
        },
      },
      {
        path: "resume-preview-three",
        name: "RESUMEPREVIEWTHREE",
        isHidden:true,
        component: () =>
          import("@/views/resume-guidance/components/resume-preview-three.vue"),
        meta: {
          icon: "",
          title: "简历预览THREE ",
        },
      },

      {
        path: "resume-edit-one",
        name:"RESUMEEDITONE",
        isHidden:true,
        component: () =>
          import("@/views/resume-guidance/components/resume-edit-one.vue"),
        meta: {
          icon: "",
          title: "简历编辑one",
        },
      },

      {
        path: "resume-edit-two",
        name:"RESUMEEDITTWO",
        isHidden:true,
        component: () =>
          import("@/views/resume-guidance/components/resume-edit-two.vue"),
        meta: {
          icon: "",
          title: "简历编辑two",
        },
      },

      {
        path: "resume-edit-three",
        name:"RESUMEEDITTHREE",

        component: () =>
          import("@/views/resume-guidance/components/resume-edit-three.vue"),
        isHidden:true,
        meta: {
          icon: "",
          title: "简历编辑three",
          
        },
      },
    ],
  },
  //简历模块路由结束

  //  面试通知页面路由
  {
    path: "notify",
    name: "NOTIFY",
    component: notify,
    meta: {
      icon: "",
      title: "面试通知",
    },
  },
  // 面试题页面路由
  {
    path: "interview-question",
    name: "INTERVIEWQUESTION",
    component: () => import("@/views/interview-question/Index.vue"),
    meta: {
      icon: "",
      title: "笔试/面试题录入",
    },
  },
  // 新增公司路由
  {
    path: "company",
    name: "COMPANY",
    component: () => import("@/views/company/Index.vue"),
    meta: {
      icon: "",
      title: "公司信息",
    },
  },
  {
    path: "register-company",
    name: "REGISTERCOMPANY",
    component: () => import("@/views/company/RegisterCompany.vue"),
    isHidden: true,
    meta: {
      icon: "",
      title: "注册公司",
    },
  },
  {
    path: "company-info/:id",
    name: "COMPANYINFO",
    component: () => import("@/views/company/CompanyInfo.vue"),
    isHidden: true,
    meta: {
      icon: "",
      title: "公司详情",
    },
  },
  {
    path: "publish-job/:id",
    name: "PUBLISHJOB",
    component: () => import("@/views/job/PublishJob.vue"),
    isHidden: true,
    meta: { 
      icon: "",
      title: "发布职位",
    },
  },
  // 工作详情页面路由
  {
    path: "/job-detail",
    name: "JOBDETAIL",
    isHidden: true,
    component: () => import("@/views/job/Job-detail.vue"),
  },
    //公司人员检阅简历路由
    {
      path: "/checkresume",
      name: "CHECKRESUME",
      component: () => import("@/views/company/CheckResume.vue"),
      isHidden: true,
      meta: {
        icon: "",
        title: "检阅简历",
      },
    },
  
]

export default authRoutes
