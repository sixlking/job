/*
 * @Author: qianduanxuyaochendian lxy_01@outlook.com
 * @Date: 2022-05-13 21:00:20
 * @LastEditors: qianduanxuyaochendian lxy_01@outlook.com
 * @LastEditTime: 2022-05-14 17:05:27
 * @FilePath: \job\web\src\router\basicRoutes.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
const basicRoutes = [
  {
    path: "/",
    name: "LAYOUT",
    component: () => import("@/layout/Index.vue"),
    redirect: "/job-square",
    children: [],
  },
  {
    path: "/login",
    name: "LOGIN",
    component: () => import("@/views/login/Index.vue"),
    meta: {
      icon: "",
      title: "登录",
    },
  },
  {
    path: "/job-square",
    name: "JOBSQUARE",
    component: () => import("@/views/job-square/Index.vue"),
    meta: {
      icon: "",
      title: "招聘广场",
    },
  },
  {
    path: "/register",
    name: "REGISTER",
    component: () => import("@/views/register/Index.vue"),
    meta: {
      icon: "",
      title: "注册",
    },
  },
  {
    name: "404",
    path: "/404",
    component: () => import("@/views/error/404.vue"),
    isHidden: true,
  },
  {
    name: "403",
    path: "/403",
    component: () => import("@/views/error/403.vue"),
    isHidden: true,
  },
  {
    name: "500",
    path: "/500",
    component: () => import("@/views/error/500.vue"),
    isHidden: true,
  },
]

export default basicRoutes
